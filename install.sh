#!/bin/sh
sudo apt update
sudo apt -y upgrade
sudo apt -y install python3-pip mpg123 espeak gpiozero
pip3 install -r requirements.txt
sudo cp wonderfoon-python.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable wonderfoon-python.service
sudo mkdir -p /usr/share/wonderfoon/music/
sudo chown -R pi /usr/share/wonderfoon
sudo cp dial.mp3 /usr/share/wonderfoon
sudo systemctl start wonderfoon-python.service
