#!/usr/bin/python3

import subprocess	#start music and dialtone
import gpiozero		#gpio module
import math			#calculate dialed number
import os			#file access
import time			#time and sleep
import json			#json for configs

pin_rotaryenable = 26	# yellow
pin_countrotary = 19		# red
pin_hook = 16
button_hook = 12

rotaryenable = gpiozero.Button(pin_rotaryenable)
countrotary = gpiozero.Button(pin_countrotary)
hook = gpiozero.Button(pin_hook)
button = gpiozero.Button(button_hook)

def readconfig():
	print("Read config")
	with open('/etc/wonderfoon/config.json') as config_file:
		config = json.load(config_file)
	subprocess.Popen(["/usr/bin/amixer", "cset", "numid=1",  str(config["Volume"]) + "%"])
	return config

def readmusicconfig():
	print("Read musicconfig")
	with open('/etc/wonderfoon/music.json') as music_file:
		music_config = json.load(music_file)
	return music_config

def shutdown():
	print("Shutting down!")
	subprocess.Popen(["/usr/bin/espeak", "shutting down now"])
	subprocess.Popen(["sudo", "shutdown", "-h", "now"])

def getip():
	print("Getting the IP")
	try:
		ip = subprocess.check_output(['/bin/hostname', '-I']).rstrip()
		subprocess.Popen(["/usr/bin/espeak", "-v", "dutch", "Het IP is " + str(ip).replace(".", " ")])
	except:
		subprocess.Popen(["/usr/bin/espeak", "No IP found"])

class Dial():
	def __init__(self):
		print("Initializing...")
		self.pulses = 0
		self.number = ""
		self.counting = True
		self.calling = False

	def startcalling(self):
		print("Start calling")
		self.reset()
		self.calling = True
		self.player = subprocess.Popen(["mpg123", "--loop", "20", "-q", "/usr/share/wonderfoon/dial.mp3"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	def stopcalling(self):
		print("Stop calling")
		self.calling = False
		self.reset()

	def startcounting(self):
		print ("Start counting")
		self.counting = self.calling

	def stopcounting(self):
		print("Stop counting")
		if self.calling:
			print ("Got %s pulses.." % self.pulses)
			if self.pulses > 0:
				if math.floor(self.pulses / 2) == 10:
					self.number += "0"
				else:
					short_num = str(math.floor(self.pulses/2))
					self.number += short_num
			print("Than %s is dialed!" % self.number)
			self.pulses = 0
			config = readconfig()
			music_list = readmusicconfig()
			if self.number == config["Shutdown"]:
				shutdown()
			elif self.number == config["GetIP"]:
				try:
					self.player.kill()
				except:
					pass
				getip()
			elif self.number in music_list:
				print("Found number in playlist, playing: " + music_list[self.number])
				print("start player with number = %s" % self.number)
				try:
					self.player.kill()
				except:
					pass
				self.player = subprocess.Popen(["mpg123", config["MusicDir"] + music_list[self.number], "-q"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				self.counterreset
			else:
				print("No idea what to do...")
		# Wonderfoon mode? Accept only 1 digit songs unless button is pushed
		if config["Mode"] == "wonderfoon" and button.value == 0:
			print("Wonderfoon mode, resetting dailer")
			self.counterreset()
		else:
			print("Escaperoom mode, nothing to do")
		self.counting = False

	def addpulse(self):
		print("Add pulse")
		if self.counting:
			print("real addpulse")
			self.pulses += 1

	def getnumber(self):
		return self.number

	def counterreset(self):
		print("Reset counter")
		self.pulses = 0
		self.number = ""
		
	def reset(self):
		print("Reset all")
		self.counterreset()
		try:
			self.player.kill()
		except:
			pass

if __name__ == "__main__":
	dial = Dial()
	countrotary.when_deactivated = dial.addpulse
	countrotary.when_activated = dial.addpulse
	rotaryenable.when_activated = dial.startcounting
	rotaryenable.when_deactivated = dial.stopcounting
	hook.when_activated = dial.stopcalling
	hook.when_deactivated = dial.startcalling
	while True:
		time.sleep(1)